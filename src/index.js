import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './App'

import './sb-admin-2.css';

import { Provider } from 'react-redux'
import {store, persistor} from './redux/store'
import { PersistGate } from 'redux-persist/integration/react'


ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <App style={{backgroundColor: '#f8f9fc'}} />
        </PersistGate>
    </Provider>
, document.getElementById('root'));
