import React from 'react'
import { getDishes } from './redux/actions/dishesActions'
import { useDispatch, useSelector } from 'react-redux'
import {Dishes} from './components/front-office/Dishes/Dishes'
import Detail from './components/front-office/Detail/Detail'
import CheckList from './components/front-office/CheckList/CheckList'
import Preparation from './components/front-office/Preparation/Preparation'
import BackOffice from './components/back-office/BackOffice';
import Landing from './components/front-office/Layout/Landing'
import Login from './components/front-office/userManagement/Login'
import Register from './components/front-office/userManagement/Register'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import '../src/components/notifications/notifications.css';
import { NotificationContainer } from 'react-notifications';
import jwt_decode from 'jwt-decode'
import setJWTToken from './securityUtils/setJWTToken'
import {setCurrentUser} from './redux/actions/authActions'
import {logoutUser} from './redux/actions/authActions'
import SecuredRoute from './securityUtils/securedRoute'
import LandingBrowser from './components/front-office/Layout/LandingBrowser'
import {

    MobileView,
    isBrowser
  } from "react-device-detect";
export const App = () => {

    const dispatch = useDispatch()
    const jwtToken = localStorage.getItem("JwtToken")
        
    if (jwtToken) {
        setJWTToken(jwtToken)
        const decoded_jwtToken = jwt_decode(jwtToken)
        dispatch(setCurrentUser(decoded_jwtToken))

        const currentTime = Date.now() / 1000;

        if(decoded_jwtToken.exp < currentTime) {
            setJWTToken(false)
            dispatch(logoutUser())
            window.location.href="/";
        }
    }

    const validToken = useSelector(state => state.auth.validToken)
        if(validToken) {
            fetch('/api/v1/dishes')
            .then(response => { return response.json() })
            .then(data => {
                dispatch(getDishes(data.data))
            })
        }
        if (isBrowser) {
            return <LandingBrowser />
        }
    return (
        <Router>
            <Switch>
                {/* PUBLIC ROUTES */}
                <Route exact path="/" component={Landing || LandingBrowser} />
                
                <MobileView>
                    <Route path="/creer-mon-compte" component={Register} />
                    <Route path="/me-connecter" component={Login} />
                    {/* PRIVATE ROUTES */}
                    <SecuredRoute exact path="/mes-assiettes" component={() => <Dishes />} />
                    <SecuredRoute path="/mon-assiette/:id" component={() => <Detail />} />
                    <SecuredRoute path="/ma-checklist/:id" component={() => <CheckList />} />
                    <SecuredRoute path="/preparation/:id" component={() => <Preparation />} />
                    <SecuredRoute path="/back-office" component={() => <BackOffice />} />
                    </MobileView>
            </Switch>
                <NotificationContainer />
        </Router>
    )
}