import { combineReducers } from 'redux';
import { dishesReducer } from './reducers/dishesReducer'
import {dishReducer} from './reducers/dishReducer'
import { authReducer } from './reducers/authReducer'
import {errorReducer} from './reducers/errorReducer'

export default combineReducers({
    dishes: dishesReducer, 
    dish: dishReducer,
    errors: errorReducer, 
    auth: authReducer
})