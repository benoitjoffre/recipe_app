const initialState = {
    user: {},
    validToken: false
}
// const booleanActionPayload = (payload) => {
//     if(payload) {
//         return true
//     } else {
//         return false
//     }
// }
export const authReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'SET_CURRENT_USER':
            return {
                ...state,
                validToken: true,
                user: action.payload
            }
        case 'LOGOUT_USER':
            return {
                ...state,
                validToken: false,
                user: action.payload
            }
            default:
                return state
    }
}