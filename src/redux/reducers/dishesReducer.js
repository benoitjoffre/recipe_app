
export const dishesReducer = (state = [], action) => {
    switch(action.type) {
        case 'GET_DISHES':
            return action.dishes
        case 'ADD_DISH_ITEM': 
            return [...state, action.payload]
        case 'REMOVE_DISH_ITEM': 
            return state.filter(item => item.id !== action.payload.id)
        default: 
            return state
    }
}