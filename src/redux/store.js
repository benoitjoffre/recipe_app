import {createStore, applyMiddleware, compose} from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import rootReducer from './index.js';


const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['dishes', 'dish']
}
// eslint-disable-next-line
  const dishesPersistConfig = {
    key: 'dishes',
    storage: storage
  }
  // eslint-disable-next-line
  const dishPersistConfig = {
    key: 'dish',
    storage: storage
  }


  const middleware = [thunk];
  const persistedReducer = persistReducer(persistConfig, rootReducer)

  export let store = createStore(persistedReducer,compose(applyMiddleware(...middleware)
  // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
  )
  export let persistor = persistStore(store)

