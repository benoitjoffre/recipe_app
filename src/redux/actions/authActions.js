export const setCurrentUser = decoded => ({
    type: 'SET_CURRENT_USER',
    payload: decoded
})

export const logoutUser = () => (
    // eslint-disable-next-line
    localStorage.removeItem("JwtToken"),
    {
        type: 'LOGOUT_USER',
        payload: {}
    }
)