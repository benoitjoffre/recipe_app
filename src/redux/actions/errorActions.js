export const getErrors = errors => ({
    type: 'GET_ERRORS',
    payload: errors
})