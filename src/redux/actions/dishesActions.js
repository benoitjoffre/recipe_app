export const getDishes = dishes => ({
    type: 'GET_DISHES',
    dishes
})

export const addDishItem = dishItem => ({
    type: 'ADD_DISH_ITEM',
    payload: dishItem
})

export const removeDishItem = dishItem => ({
    type: 'REMOVE_DISH_ITEM',
    payload: dishItem
})