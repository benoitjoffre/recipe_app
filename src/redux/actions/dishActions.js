export const setCurrentDish = dish => ({
    type: "SET_CURRENT_DISH",
    dish
})