import React from 'react'
import {Route} from 'react-router-dom'
import {useSelector} from 'react-redux'
import Login from '../components/front-office/userManagement/Login'


const SecuredRoute = ({ component, ...options }) => {

    const auth = useSelector(state => state.auth)

    const finalComponent = auth.validToken ? component : Login;

    return <Route {...options} component={finalComponent} />
}

export default SecuredRoute;