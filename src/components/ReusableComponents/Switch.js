import React from "react";
import { useId } from "react-id-generator";
 
const Switch = ({ children, ...rest }) => {
  const [htmlId] = useId();
 
  return (
    <>       
      <input id={htmlId} className="custom-control-input" type="checkbox"  {...rest} />
      <label htmlFor={htmlId} className="custom-control-label" />

    </>
  )
}
export default Switch;