import React from 'react'

export const SearchBar = ({value, onChange}) => <> 

<div className="input-group animated--grow-in" style={{marginRight: '20px'}}>
            <input value={value} onChange={onChange} className="form-control bg-light m-0 border-0 small" placeholder="Rechercher un plat.." />
            <div className="input-group-append">
                <button className="btn btn-primary">
                    <i className="fas fa-search fa-sm" />
                </button>
            </div>
</div>
</>