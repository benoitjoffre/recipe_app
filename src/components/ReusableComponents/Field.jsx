import React from 'react'

export const Field = ({ label, name, value, type, onChange }) => <>
    {label && <label htmlFor={name}>{label}</label>}
    <input className="form-control" name={name} type={type} value={value} onChange={onChange} />
</>