import React from 'react'
import {useHistory} from 'react-router-dom'
import './index.scss'
const Header = () => {
    const history = useHistory()

    return (
        <div className="navbar-light header-wrapper bg-light mb-4 shadow">
            <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                    <button onClick={() => history.goBack()} className="btn btn-border-primary">
                    <i className="fas fa-chevron-left" />
                    </button>
                </li>
            </ul>
        </div>
    )
}

export default Header