import React from 'react'
import {logoutUser} from '../../../redux/actions/authActions'
import {useSelector} from 'react-redux'

import './index.scss'
const HeaderDishes = () => {
    const user = useSelector(state => state.auth.user)
    const logout = () => {
        logoutUser()
        window.location.href='/'
    }

    return (
        <div className="navbar-light header-wrapper bg-light mb-4 shadow">
            <ul className="navbar-nav w-100 d-flex flex-row justify-content-between">
                <li className="my-auto">Let's cooking {user.name} !</li>
                <li className=" my-auto">
                <button alt="Deconnexion"className="btn btn-border-sign-out" onClick={logout}>
                    <i className="fas fa-sign-out-alt" />
                </button>
                </li>
            </ul>
        </div>
    )
}

export default HeaderDishes