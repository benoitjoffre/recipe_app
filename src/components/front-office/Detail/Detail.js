import React, {useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {Link} from 'react-router-dom'
import Header from '../Header/header'
import {useParams} from 'react-router-dom'
import {setCurrentDish} from '../../../redux/actions/dishActions'
import './index.scss'
const Detail = () => {

    const {id} = useParams()
    const dispatch = useDispatch()
    useEffect(() => {
        fetch('/api/v1/dishes/' + id)
        .then(response => { return response.json() })
        .then(data => {
            dispatch(setCurrentDish(data))
        })
        // eslint-disable-next-line
    }, [])
                 
    const currentDish = useSelector(state => state.dish[0])
    console.log(currentDish)
    return (
        <>
        <Header />
        <div style={{backgroundImage: `url(${currentDish.dishImgUrl})`, width:'8rem', minHeight: '8rem', backgroundPosition: 'center', backgroundSize: 'cover', backgroundRepeat: 'no-repeat', fontSize: '30px', boxShadow: 'inset 2000px 0 0 0 rgba(0, 0, 0, 0.5)'}} className="card w-100 d-flex flex-row align-items-center justify-content-center">
            <h1 className="text-center text-white">{currentDish.name}</h1>
        </div>
        <div className="container detail-wrapper">
            <div className="row">
                <div className="col">
                    <div className="card border-left-primary shadow">
                        <Link className="font-weight-bold text-gray-800" to={"/ma-checklist/" + currentDish._id}>Check List</Link>
                    </div>
                </div>
                <div className="col">
                    <div className="card border-left-success shadow">
                        <Link className="font-weight-bold text-gray-800" to={"/preparation/" + currentDish._id}>Préparation</Link>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}
export default Detail