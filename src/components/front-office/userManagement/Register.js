import React, {useState, useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import { useHistory, Link } from "react-router-dom";
import axios from 'axios'
import {getErrors} from '../../../redux/actions/errorActions'
import { NotificationManager } from 'react-notifications';

const Register = () => {

    const dispatch = useDispatch();
    const [register, setRegister] = useState({name: "", email: "", password: "", password2: ""});
    const history = useHistory();


    const handleChange = (e) => {
        setRegister({...register, [e.target.name]: e.target.value})
    }


    const validToken = useSelector(state => state.auth.validToken)
    useEffect(() => {
        if(validToken) {
            history.push('/mes-assiettes')
        }
    })

    const handleSubmit =(e) => {
        e.preventDefault();
        
        axios.post("/api/v1/users/register", register)
        .then(function (response) {
            console.log(response)
            history.push('/me-connecter');
            NotificationManager.success('Vous avez créé votre compte', 'Félicitation !', 3000);
        })
        .catch(function (err) {
            console.log(err)
            dispatch(getErrors(err.response.data))
        })
    }

    const errors = useSelector(state => state.errors)
          
    return (
        <div className="container">
            <div className="card o-hidden border-0 shadow-lg my-5">
                <div className="card-body p-5">
                    <h3 className="text-center mb-4">Créer mon compte</h3>
                    <form onSubmit={handleSubmit} className="user">
                        <div className="form-group">
                            <input className="form-control form-control-user" name={'name'} type={'text'} value={register.name} onChange={handleChange} placeholder={'Nom complet'} />
                            {errors.name && (<div className="invalid-feedback">{errors.name}</div>)}
                        </div>

                        <div className="form-group">
                            <input className="form-control form-control-user" name={'email'} type={'email'} value={register.email} onChange={handleChange} placeholder={'E-mail'} />
                            {errors.email && (<div className="invalid-feedback">{errors.email}</div>)}
                        </div>

                        <div className="form-group">
                            <input className="form-control form-control-user" name={'password'} type={'password'} value={register.password} onChange={handleChange} placeholder={'Mot de passe'} />
                            {errors.password && (<div className="invalid-feedback">{errors.password}</div>)}
                        </div>

                        <div className="form-group">
                            <input className="form-control form-control-user" name={'password2'} type={'password'} value={register.password2} onChange={handleChange} placeholder={'Confirmation du mot de passe'} />
                            {errors.password2 && (<div className="invalid-tooltip">{errors.password2}</div>)}
                        </div>

                        <button className="btn btn-primary btn-user btn-block">Créer mon compte</button>
                        <div className="text-center mt-4">
                            <Link className="small" to="/me-connecter">Tu as déjà un compte ? </Link>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default Register;