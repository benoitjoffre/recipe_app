import React, {useState, useEffect} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {getErrors} from '../../../redux/actions/errorActions'
import {setCurrentUser} from '../../../redux/actions/authActions'
import axios from 'axios'
import { NotificationManager } from 'react-notifications';
import { useHistory, Link } from "react-router-dom";
import setJWTToken from '../../../securityUtils/setJWTToken'
import jwt_decode from 'jwt-decode'

const Login = () => {
    const [login, setLogin] = useState({email: "", password: ""});
    const history = useHistory();
    const dispatch = useDispatch()

    const handleChange = (e) => {
        setLogin({...login, [e.target.name]: e.target.value})
    }
    const validToken = useSelector(state => state.auth.validToken)

    useEffect(() => {
        if(validToken) {
            history.push('/mes-assiettes')
        }

    })
    const handleSubmit = (e) => {
        e.preventDefault();
        
        axios.post("/api/v1/users/login", login)
        .then(function (response) {
            const {token} = response.data
            localStorage.setItem("JwtToken", token)
            setJWTToken(token)

            const decoded = jwt_decode(token)
            dispatch(setCurrentUser(decoded))
            history.push('/mes-assiettes');
            NotificationManager.success('Tu es maintenant connecté', 'Bienvenue !', 3000);
        })
        .catch(function (err) {
            
            dispatch(getErrors(err.message))
            
        })
    }

    const errors = useSelector(state => state.errors)
   

    return (
        <div className="container my-auto">
            <div className="card o-hidden border-0 shadow-lg my-5">
                <div className="card-body p-5">
                    <h3 className="text-center mb-4">Connexion</h3>
                    <form onSubmit={handleSubmit} className="user">

                        <div className="form-group">
                            <input className="form-control form-control-user" name={'email'} type={'email'} value={login.email} onChange={handleChange} placeholder={'Adresse e-mail'} />
                            { errors.email && (<div className="invalid-feedback">{errors.email}</div>)}
                        </div>

                        <div className="form-group">
                            <input className="form-control form-control-user" name={'password'} type={'password'} value={login.password} onChange={handleChange} placeholder={'Mot de passe'} />
                            {errors.password && (<div className="invalid-feedback">{errors.password}</div>)}
                        </div>

                        <button className="btn btn-primary btn-user btn-block mt-4">Je me connecte</button>
                        <div className="text-center mt-4">
                            <Link className="small" to="/creer-mon-compte">Tu n'as pas encore de compte ? </Link>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default Login;