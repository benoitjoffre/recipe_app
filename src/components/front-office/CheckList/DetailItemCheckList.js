import React, {useState} from 'react'
import Switch from '../../ReusableComponents/Switch'
const DetailItemCheckList = (props) => {
    
    const [checked, setChecked] = useState(false)

    return (
        <div className="d-flex w-100 mb-4">
            <div style={{height: '2.1rem', marginTop: '4px'}} className={` card border-left-primary shadow mr-2 w-100 d-flex justify-content-center text-center ${checked === true ? 'list-group-item-dark': ''}`}>
            {props.ingredients.quantity + ' '}{props.ingredients.ingredientName}
            </div>
            <div className="custom-control custom-switch ">
                <Switch defaultChecked={checked} onClick={() => setChecked(!checked)} />
            </div>
        </div>
    )
}

export default DetailItemCheckList;