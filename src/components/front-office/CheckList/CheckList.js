import React from 'react';
import {useSelector} from 'react-redux'
import DetailItemCheckList from './DetailItemCheckList'
import Header from '../Header/header'


const CheckList = (props) => {
 
	const currentDish = useSelector(state => state.dish[0])
			
        return (
			<>
			<Header />
			<div className="container">
				<h2 className="text-center">Ma Check List</h2>
				<p className="text-center">-</p>
				<h3 className="text-center">{currentDish.name}</h3>

				<div className="mt-4">
				{currentDish.ingredients.map(ingredient => (
						<DetailItemCheckList key={ingredient.ingredientName} ingredients={ingredient} />
				))}
				</div>
			</div>
			</>
        )

}

export default CheckList;