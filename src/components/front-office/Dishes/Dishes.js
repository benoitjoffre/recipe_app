import React, {useState, useEffect} from 'react'
import './index.scss'
import {Link} from 'react-router-dom'
import {useSelector} from 'react-redux'
import HeaderDishes from '../Header/headerDishes'
import {SearchBar} from '../../ReusableComponents/SearchBar'
export const Dishes = () => {
    
    const dishes = useSelector(state => state.dishes)
    const [searchTerm, setSearchTerm] = useState("");
    const [searchResults, setSearchResults] = useState([]);

    const handleChange = event => {
        setSearchTerm(event.target.value);
    };

    useEffect(() => {
        const results = dishes.filter(dish =>
            dish.name.toLowerCase().includes(searchTerm.toLowerCase())
        );
        setSearchResults(results);
        // eslint-disable-next-line
      }, [searchTerm]);

    return (
        <>
        <HeaderDishes />
        <div className="dishes-wrapper">
            <div className="container">
                <SearchBar value={searchTerm} onChange={handleChange} />
            { searchResults.map(item => (
                <div className="mt-4 animated--grow-in" key={item._id} >
                    <Link style={{textDecoration: 'none', color: 'white'}} detail={item} to={"/mon-assiette/" + item._id}>
                        <div style={{backgroundImage: `url(${item.dishImgUrl})`}} className="card w-100 card-dish"  detail={item}>
                            <div className="row my-auto">
                                <div className=" col my-auto">
                                        {item.name}
                                </div>
                            </div>
                        </div>
                    </Link>
                </div>
            ))}
            </div>
        </div>
        </>
    )
}