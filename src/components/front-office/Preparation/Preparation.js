import React from 'react'
import {useSelector} from 'react-redux'
import Header from '../Header/header'
const Preparation = () => {
   
    const currentDish = useSelector(state => state.dish[0])
   
    
    return(
        <>
                <Header />
                <h2 className="text-center">Ma Préparation</h2>
        {currentDish.preparation.map(prepa => (
            <div className="preparation-wrapper" key={prepa.etape}>
                <div className="container">
                    <div className="card border-left-primary shadow mb-4 p-2" >
                        <div className="row d-flex align-items-center">
                            <div className="col-2 mr-2">
                                <span className="btn btn-primary btn-circle ml-2">
                                    {prepa.etape + 1}
                                </span>
                            </div>
                            <div className="col-8">
                                <p style={{marginBottom: '0px'}}>{prepa.description}</p>
                            </div>
                        </div>
                    
                    </div>
                </div>
            </div>
            
        ))}
        </>
    )
}

export default Preparation;