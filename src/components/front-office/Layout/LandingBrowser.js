import React from 'react'


const LandingBrowser = () => {

    
    return (
        <div className="container p-4 d-flex flex-column my-5">
            <h3 className="text-center mb-5">Bienvenue sur cook'App</h3>
            <h5 className="text-center mb-5">Tu oublies toujours quelque chose en faisant tes courses ? <br /> Tu ne sais pas quoi cuisiner ? <br /> Essaie Cook'App !</h5>
            {/* <p className="text-center mb-5">Le site est en cours de construction...</p> */}
            <h4 className="text-center">Ce site est disponible seulement sur mobile !</h4>
        </div>
    )
}
export default LandingBrowser;