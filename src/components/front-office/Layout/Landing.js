import React, {useEffect} from 'react'
import {useSelector} from 'react-redux'
import { Link } from 'react-router-dom'
import {useHistory} from 'react-router-dom'

const Landing = () => {

    const history = useHistory();
    const validToken = useSelector(state => state.auth.validToken)

    useEffect(() => {
        if(validToken) {
            history.push('/mes-assiettes')
        }
    })
    
    return (
        <div className="container p-4 d-flex flex-column my-5">
            <h3 className="text-center mb-5">Bienvenue sur cook'App</h3>
            <h5 className="text-center mb-5">Tu oublies toujours quelque chose en faisant tes courses ? <br /> Tu ne sais pas quoi cuisiner ? <br /> Essaie Cook'App !</h5>
            <p className="text-center mb-5">Le site est en cours de construction...</p>
            <div className="row d-flex justify-content-center">
                <Link to="/me-connecter" style={{borderRadius: '10rem'}} className="btn btn-lg btn-primary w-100 mb-4">Se connecter</Link>
                <Link to="/creer-mon-compte" style={{borderRadius: '10rem'}} className="btn btn-lg btn-secondary w-100">Créer un compte</Link>
            </div>
        </div>
    )
}
export default Landing