import React, {useState} from 'react';
import axios from 'axios'
import {NotificationManager} from 'react-notifications'
import {useSelector} from 'react-redux'

const LikeButton = (props) => {

    const dish = useSelector(state => state.dishes)
    const [likes, setLikes] = useState(dish.likes)
    const [isLiked, setIsLiked] = useState(false)

    const handleChange = () => {
        
        if(isLiked === false ) {
            setIsLiked(true)
            setLikes(likes + 1)
            const {_id} = props.passData;
            axios.put("/api/v1/dishes/" + _id, {likes: likes})
                .then(function (response) {
                    console.log(response)
                    if (!isLiked) {NotificationManager.info('Tu as liké ce plat', 'Félicitation !', 3000);} 
                })
                .catch(function (err) {
                    console.log(err)
                
                })
        } else if (isLiked === true) {
            setLikes(likes -1)
            setIsLiked(false)
            const {_id} = props.passData;
            axios.put("/api/v1/dishes/" + _id, {likes: likes})
                .then(function (response) {
                    console.log(response)
                    if (!isLiked) {NotificationManager.info('Tu as liké ce plat', 'Félicitation !', 3000);} 
                })
                .catch(function (err) {
                    console.log(err)
                
                })
        }
    }

return (
    <>
    <i onClick={handleChange} style={{color: '#e74a3b', cursor: 'pointer'}} className={isLiked === false ? `far fa-heart ml-4`: `fas fa-heart ml-4`} />
    </>
)
}
export default LikeButton;