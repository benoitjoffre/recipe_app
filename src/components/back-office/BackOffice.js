import React from 'react';
import { useSelector } from 'react-redux'
import {Link} from 'react-router-dom'
import ItemList from './itemList'
import { AddRecipeForm } from './AddRecipeForm';


const BackOffice  = () => {

  const recipes = useSelector(state => state.dishes) 
  return (
  <div className="">
    <h3 className="text-center">Les plats</h3>
    <Link className="btn btn-primary ml-4" to="/mes-assiettes">Aller vers l'app</Link>
    <AddRecipeForm />
    
    <div className="w-100">
      {recipes.length !== 0 ? (
    <table className="table table-bordered mb-4 w-100" style = {{ borderSpacing: '0px'}}>
            <thead className="thead-light">
                <tr>
                    <th scope="col">Plat</th>
                    <th scope="col">Ingrédients</th>
                    <th scope="col">Supprimer</th>
                </tr>
            </thead>
      { recipes.map(item => (
          <ItemList key={item._id} details={item} />
      ))}
      </table>
      ) : (
        <h3 className="text-center">Commencez par ajouter un ingrédient.</h3>
      )}
          {(localStorage.getItem("urlImg") !== null ? 
      (
        <div className="alert alert-success alert-dismissible fade show mt-4" role="alert">
            <strong>Choisissez</strong> une image
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
            </button>
		</div>
      ) : null
    )}
    </div>
  </div>
  )

}


export default BackOffice;
