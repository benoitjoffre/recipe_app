import React from 'react'

export const FieldGroup = ({ name, children }) => 
<fieldset>
    {name && <legend>{name}</legend>}{children}
</fieldset>