import React, {useState} from 'react'


const ImageUploader = (props) => {
    const MY_ACCESS_KEY = "29a5fbc99bc421297e12c0cc254b5e765e139ab86bfd40dcba86171247c5e1ee"

    // State de la recherche
    const [search, setSearch] = useState("");

    // State des images en réponse de la requête
    const [image, setImage] = useState([{urls : {
        small: ""
    }}]);
    
    // State final avec l'url de l'image
    // eslint-disable-next-line
    const [urlImg, setUrlImg] = useState({url: "", isClicked: false});

    // Pour afficher les images seulement si c'est true, sinon ca affiche une image vide par defaut..
    const [show, setShow] = useState(false)

    const url = "https://api.unsplash.com/search/photos?client_id="

    const handleChange = (e) => {
        setSearch(e.target.value)
    }

  const onSearch = () => {
    fetch(
        `${url}${MY_ACCESS_KEY}&query=${search}&page=${
          1
        }`
      )
        .then(res => res.json())
        .then(res => {
            setImage(res.results)
            setShow(true) })
        .catch(err => console.log("error occured" + err));
  }

        
        


  return (
   <div className="d-flex flex-wrap">
           <label className="ml-4 mb-0">Rechercher une image</label>
       <div className="input-group m-4">
        <input className="form-control" onChange={handleChange}/>
        <div className="input-group-append">
            <span className="btn btn-secondary" onClick={onSearch}>Search</span>
        </div>
       </div>
    
<div className="d-flex flex-wrap justify-content-center">
{/* eslint-disable-next-line */}
        { image.slice(0, 4).map(img => {
            if (show === true) {
              return (<span key={img.urls.small}>
                        <img style={{cursor: 'pointer'}} className="m-2 rounded" src={img.urls.small} width={'150px'} height={'150px'} onClick={() => setUrlImg({url: img.urls.small, isClicked: true})} alt="plat" />
                      </span>)
            } 
            })}
</div>
    
   </div>
  )
}

export default ImageUploader;