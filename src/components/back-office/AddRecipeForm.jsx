import React from 'react';
import axios from 'axios';
import { useDispatch } from 'react-redux'
import { addDishItem } from '../../redux/actions/dishesActions'
import IngredientList from './IngredientList'
import { NotificationManager } from 'react-notifications';

export const AddRecipeForm = () => {
    const dispatch = useDispatch()
  
    const onAddIngredients = (dish) => {
        console.log(dish)
        
        axios.post('/api/v1/dishes', dish)
        .then(function (response) {
            dispatch(addDishItem(dish))
            NotificationManager.success('Vous avez ajouté un nouveau plat', 'Félicitation !', 3000);
            console.log(response)
            // localStorage.removeItem("urlImg");
        })
        .catch(function (error) {
            // console.log(error)
        })
    }

    return (
        <div className="m-4">
                <IngredientList addValues={onAddIngredients} />
        </div>
    )
}
