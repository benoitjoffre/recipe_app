import React from 'react'

export const Button = ({ type, onClick, children }) => <button type={type} onClick={onClick}>{children}</button>