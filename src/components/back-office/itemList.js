import React from 'react';
import { useDispatch } from 'react-redux'
import { removeDishItem } from '../../redux/actions/dishesActions'

import { NotificationManager } from 'react-notifications';

const ItemList = (props) => {
    const dispatch = useDispatch()

    const handleClick = dishId => {
        const requestOptions = {
          method: 'DELETE'
        };
      
        fetch("/api/v1/dishes/" + dishId, requestOptions).then((response) => {
            return response.json();
          }).then((result) => console.log(result))
          dispatch(removeDishItem(props.details))
          NotificationManager.error('Tu viens de supprimer un plat', 'Damn !', 3000);

      }

    return (
            <tbody>
                <tr className="p-2">
                    <td>{props.details.name}</td>
                    <td>{props.details.ingredients.map(item => `${item.quantity + ' ' + item.ingredientName}, `)}</td>
                    
                    <td>
                        
                            <button onClick={() => handleClick(props.details._id)} className="btn btn-danger">Delete</button>
                        
                    </td>
                </tr>
            </tbody>
    )
}

export default ItemList;