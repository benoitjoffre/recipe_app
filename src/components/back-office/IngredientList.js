import React from 'react'
import { Formik,Form, Field, FieldArray } from 'formik';
import ImageUploader from './ImageUploader'
const IngredientList = (props) => 
{

let urlImg = localStorage.getItem("urlImg");
    return (
        <div>
       
        <Formik
          initialValues={{ name: "", origin:"", ingredients: [], url: urlImg, likes: {} }}
          onSubmit = { (values, {resetForm}) => {
            
            props.addValues(values)
           resetForm()
           }}
        >
            {({values}) => (
            <Form>
              <label>Nom du plat :</label>
                <Field placeholder="Ajoutez un nom de plat" className="form-control mb-4" name="name" />
                <label>Origine du plat :</label>
                <Field placeholder="Ajoutez une origine du plat" className="form-control mb-4" name="origin" />
                <FieldArray
                  name="ingredients"
                  render={arrayHelpers => (
                    <div>
                      {values.ingredients && values.ingredients.length > 0 ? (
                        values.ingredients.map((ingredient, index) => (
                            
                          <div className="input-group mb-4" key={index}>
                            <Field placeholder={`ingredients ${index}`} className="form-control" name={`ingredients.${index}`} value={ ingredient || ''} />
                            <div className="input-group-append">
                              <button className="input-group-text"
                                type="button"
                                onClick={() => arrayHelpers.remove(index)} // remove a ingredient from the list
                              >
                                -
                              </button>
                              <button
                              className="input-group-text"
                                type="button"
                                onClick={() => arrayHelpers.insert(index, '')} // insert an empty string at a position
                              >
                                +
                              </button>
                            </div>
                          </div>
                        ))
                      ) : (
                        <button className="btn btn-secondary w-100 mb-4" type="button" onClick={() => arrayHelpers.push('')}>
                          {/* show this when user has removed all ingredients from the list */}
                          Ajouter un ingrédient
                        </button>
                      )}
                      <div>
                        <ImageUploader name="imageUrl"/>
                        <button className="btn btn-primary w-100" type="submit">Enregistrer le plat</button>
                        </div>
                    </div>
                  )}
                />
         

            </Form>
            
            )}
          </Formik>
        
      </div>
    )

}

export default IngredientList